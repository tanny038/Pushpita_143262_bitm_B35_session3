<?php

$ab = "15.23bitm";

echo floatval($ab);

?>

<br/>

<?php

$bc = array('a','b','c');

if(is_array($bc))
    echo 'This is an array';
else
    echo 'This is not an array';

?>

<br/>

<?php

$cd = 45.5;

if(is_null($cd))
    echo 'This is null';

else
    echo 'This is not null';
?>

<br/>

<?php

$de = 'jhgvj14.45';
$ef = 12;
$fg = 34.45;
$gh = TRUE;

var_dump($de);
echo '<br/>';
var_dump($ef);
echo '<br/>';
var_dump($fg);
echo '<br/>';
var_dump($gh);
echo '<br/>';

?>

<?php

$hi = 'test';

var_dump(isset($hi));

?>

<br/>

<?php

$ij = 'test';

var_dump(empty($ij));

?>

<br/>

<?php

$jk = 'ABC';
$kl = 12;
$lm = 34.45;
$mn = TRUE;

print_r($jk);
echo '<br/>';
print_r($kl);
echo '<br/>';
print_r($lm);
echo '<br/>';
print_r($mn);
echo '<br/>';

?>

<br/>

<?php

$no = serialize(array('Princy','Pushpita','Nupur','Baishali'));

echo $no . "<br/>";

?>

<br/>

<?php

$no = serialize(array('Princy','Pushpita','Nupur','Baishali'));

echo $no . "<br/>";

$op = unserialize($no);

var_dump ($op);
echo "<br/>";

?>

<br/>

<?php

$p = 'test is over';
echo $p .'<br/>';

unset($p);
echo $p;

?>

<br/>

<?php

$d = 'jhgvj14.45';
$e = 12;
$f = 34.45;
$g = TRUE;
$h = array('a','b','c');

var_export($d);
echo '<br/>';
var_export($e);
echo '<br/>';
var_export($f);
echo '<br/>';
var_export($g);
echo '<br/>';
var_export($h);

?>

<br/>

<?php

echo gettype(12345) . '<br/>';
echo gettype('abcd') . '<br/>';
echo gettype(12.45) . '<br/>';

?>

<br/>

<?php

$k = 12345;

if(is_scalar($k))
    echo 'This is a scalar';
else
    echo 'This is not a scalar';

?>
